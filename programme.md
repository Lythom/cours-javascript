5 * 4 * 1h30

1. Algorithmique
• Notion de variable
• Manipulation des variables, Procédures, fonctions et passage de paramètres, penser entrées sorties
• Traitements conditionnés
• Structures itératives
• Tableaux

2. Concepts important de JavaScript
• intero Algo
• Bref historique et évolutions du langage
• Définitions et intérêt du langage
• Positionnement du JavaScript en technologies Web
• Rappel sur les architectures Web et le protocole http
• L’écosystème JavaScript
• L’environnement de travail
• Règles de nommage et syntaxe
• Variables et constantes
• Types de données
• Opérateurs et expressions
• Conversion de types
• Traitements conditionnés
• Traitements itératifs
• Tableaux
• Procédures et fonctions

3. Développer des sites web interactifs
• intero JS
• Intégration du JavaScript dans un document HTML
• Compatibilité des navigateurs
• Accéder au DOM
	• Rappel sur les formulaires en HTML
	• Manipulation des éléments d’un formulaire
	• Validation de formulaires
• L’objet Event et le flux d’évènement
	• Principaux évènements JavaScript
	• Le gestionnaire d’évènements
	• Poser un gestionnaire d’évènement
	• Associer un traitement à un évènement
	• Les différents modèles de gestion d’évènements

4. Créer des pages réactives
• intero site interactif
• L’objet Event et le flux d’évènement
• Notion de cookie et de stockage
• Création, lecture et manipulation des cookies
• Stockage par sessionStorage et localStorage

5. Développer et debugger
• intero pages reactives
• Bonnes pratiques
	• Organisation du code
	• Organisation des fichiers
	• Optimisation en vue des performances
• L’approche objet en JavaScript
• Gestion des erreurs
• Débogage d’un programme
• Les objets prédéfinis et leurs méthodes