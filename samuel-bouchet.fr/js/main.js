console.log('js is executed');

/**
 * Toggle d'une classe.
 * - animation CSS
 *      - Animation d'une image, d'une photo de fond,
 *      - Animation sur l'apparition du nom,
 *      - un fond écran qui apparait en quelques secondes après l'arrivee sur la page
 *      - Animation sur l'apparition du nom avec des mouvements
 *      - Apparition d'un logo quand on arrive sur le site
 *      - Animation d'une barre de compétence
 * - transition CSS
 *      - Avoir des icons qui bougent,
 * - changement de thème
 */

/*
 * Site au format slide
 * - reveal.js
 */

/**
 * Transition CSS
 */

/**
 * Animation CSS
 */

/**
 * Evénement mouseover / mouseout
 */

/**
 * Événément click
 */

/**
 * Événément scroll
 */


/**
 OnScroll events
 */
var teachingAnimated = false;
var gamesAnimated = false;
window.addEventListener('scroll', function(e) {
  var bottomOfWindow = window.scrollY + window.innerHeight;
  var MyTeachingElement = document.querySelector('#Teaching');
  if (bottomOfWindow > MyTeachingElement.offsetTop && !teachingAnimated) {
    animateLetters(MyTeachingElement);
    teachingAnimated = true;
  }

  var MyGames = document.querySelector('#VideoGames');
  if (bottomOfWindow > MyGames.offsetTop && !gamesAnimated) {
    animateLetters(MyGames);
    gamesAnimated = true;
  }
})

/**
 * Animate highlight of sections
 */
function enableHighlight(node) {
  node.classList.add("highlight");
}

function disableHighlight(node) {
  node.classList.remove("highlight");
}

var sectionArray = document.querySelectorAll('.section');
console.log(sectionArray.length + " sections trouvées");
for (var iSection = 0; iSection < sectionArray.length; iSection++) {
  sectionArray[iSection].addEventListener('mouseover', function(e) {
    enableHighlight(e.currentTarget);
  });
  sectionArray[iSection].addEventListener('mouseout', function(e) {
    disableHighlight(e.currentTarget);
  });
}

var buttonArray = document.querySelectorAll('.button');
console.log(buttonArray.length + " boutons trouvées");
for (var iButton = 0; iButton < buttonArray.length; iButton++) {
  buttonArray[iButton].addEventListener('mouseover', function(e) {
    enableHighlight(document.querySelector(e.currentTarget.getAttribute('href')).parentElement);
  });
  buttonArray[iButton].addEventListener('mouseout', function(e) {
    disableHighlight(document.querySelector(e.currentTarget.getAttribute('href')).parentElement);
  });
}

/**
 * Animate Title
 */
         // Récupère les éléments DOM qu'on souhaite animer
var textNodes = document.querySelectorAll('.animateText');
for (var iNode = 0; iNode < textNodes.length; iNode++) {
  // pour chacun, exécute la fonction d'animation
  animateLetters(textNodes[iNode]);
}

function animateLetters(textNode) {
  textNode.style.width = String(textNode.offsetWidth + 10) + "px";
// Récupère la liste des lettres
  var letters = textNode.textContent.split('');
// Vide le contenu du titre
  textNode.textContent = '';

// pour chaque lettre, créer un nœud animé
  letters.forEach((letter, i) => {
    setTimeout(() => {
      var letterElement = document.createElement('span');
      letterElement.classList.add('fromLeft');
      letterElement.textContent = letter;
      textNode.appendChild(letterElement);
    }, i * 50);
  })
}
